Feature: Display the given parameter name with a hardy Hello greeting
 Scenario: Greet the user with a given name
 When I run `chef-cutlery hello michael`
 Then the output should contain "say my name"

 Scenario: Greet the user with a given name
 When I run `chef-cutlery hello Heisenberg`
 Then the output should contain "you are goddman right"
